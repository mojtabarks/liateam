<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return view('welcome');
});

$router->get('secret', [
    'uses' => 'HomeController@secret',
    'middleware' => ['auth' , 'permission:get secret data']
]);

$router->get('users', [
    'uses' => 'UserController@index',
    'middleware' => ['auth', 'permission:grant access'],
]);

$router->post('users/grantPermission', [
    'uses' => 'UserController@grantPermission',
    'middleware' => ['auth', 'permission:grant access'],
]);

$router->group(['namespace' => 'Auth'], function () use ($router) {
    $router->post('login', 'LoginController@login');

    $router->post('register', 'RegisterController@register');

    $router->post('logout', [
        'uses' => 'LogoutController@logout',
        'middleware' => 'auth',
    ]);
});
