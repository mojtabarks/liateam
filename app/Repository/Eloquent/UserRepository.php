<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * @var Role
     */
    private $role;

    /**
     * UserRepository constructor.
     *
     * @param User|Builder $model
     * @param Role|Builder $role
     */
    public function __construct(User $model, Role $role)
    {
        parent::__construct($model);
        $this->model = $model;
        $this->role = $role;
    }

    /**
     * @param $mail
     * @return Model|null
     */
    public function findByMail($mail): ?Model
    {
        return $this->findBy('email', $mail);
    }

    /**
     * @param $request
     * @return Model
     */
    public function create($request): Model
    {
       return $this->model->create(
            $this->prepareData($request)
        );
    }

    /**
     * @param Request $request
     * @return array
     */
    private function prepareData(Request $request)
    {
        return [
            'name' => $request->input('name', Str::before($request->input('email'), '@')),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'remember_token' => Str::random(),
        ];
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->with(['roles' => function ($query) {
            /** @var Builder $query */
            $query->select('id' , 'name');
        }])->latest()
            ->get();
    }
}
