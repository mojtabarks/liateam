<?php

namespace App\Repository\Auth;

use App\Models\Role;
use App\Repository\Eloquent\UserRepository;
use App\Repository\Interfaces\RegisterRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class RegisterRepository implements RegisterRepositoryInterface
{
    protected $user = null;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var Role|Builder
     */
    private $role;

    /**
     * RegisterRepository constructor.
     * @param UserRepository $userRepository
     * @param Role $role
     */
    public function __construct(UserRepository $userRepository, Role $role)
    {
        $this->userRepository = $userRepository;
        $this->role = $role;
    }

    /**
     * @param $request
     * @return Model
     */
    public function register($request): Model
    {
        $user = $this->userRepository->create($request);
        $this->assignRole($user);
        return $user;
    }

    /**
     * @param $user
     * @return void
     */
    public function assignRole($user): void
    {
        $userRole = $this->role->where('name', 'user')->first();
        $user->assignRole($userRole);
    }

    /**
     * @param $user
     * @return string
     */
    public function createToken($user): string
    {
        return $user->createToken('Liateam\'s Lumen Grant Client')->accessToken;
    }
}
