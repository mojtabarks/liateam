<?php

namespace App\Repository\Auth;

use App\Repository\Interfaces\LogoutRepositoryInterface;

class LogoutRepository implements LogoutRepositoryInterface
{

    /**
     * @param $request
     */
    public function logout($request): void
    {
        $request
            ->user()
            ->token()
            ->revoke();
    }
}
