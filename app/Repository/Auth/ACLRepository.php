<?php

namespace App\Repository\Auth;

use App\Models\Permission;
use App\Models\Role;
use App\Repository\Interfaces\ACLRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class ACLRepository implements ACLRepositoryInterface
{
    /**
     * @var Role|Builder
     */
    private $role;
    /**
     * @var Permission|Builder
     */
    private $permission;

    /**
     * ACLRepository constructor.
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    /**
     * @param $user
     * @param $request
     */
    public function syncRole($user , $request): void
    {
        if ($role = $request->input('role')) {
            $role = $this->role->where('name' , $role)->firstOrFail();
        }

        if ($role) {
            $user->assignRole($role);
        }
    }

    /**
     * @param $user
     * @param $request
     */
    public function syncPermission($user , $request): void
    {
        if ($permission = $request->input('permission')) {
            $permission = $this->permission->where('name' , $permission)->firstOrFail();
        }

        if ($permission) {
            $user->givePermissionTo($permission);
        }
    }
}
