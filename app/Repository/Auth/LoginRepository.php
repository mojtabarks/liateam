<?php

namespace App\Repository\Auth;

use App\Exceptions\FailureResponse;
use App\Models\User;
use App\Repository\Interfaces\LoginRepositoryInterface;
use App\Repository\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginRepository implements LoginRepositoryInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * LoginRepository constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @param $user
     * @return string
     * @throws FailureResponse
     */
    public function login(Request $request, $user): string
    {
        if (!$this->canLogin($request, $user)) {
            throw new FailureResponse(
                ["message" => "username or password mismatch"],
                422
            );
        }

        // Perform login
        return $user->createToken('Lumen\'s Liateam Password Grant Client')->accessToken;
    }

    /**
     * @param Request $request
     * @param User $user
     * @return bool
     */
    private function canLogin(Request $request, User $user)
    {
        $credentialsAreOK = Hash::check($request->input('password'), $user->getAttribute('password'));

        return ($user instanceof User) && $credentialsAreOK;
    }
}
