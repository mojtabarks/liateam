<?php

namespace App\Repository\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface UserRepositoryInterface
{
    /**
     * @param $value
     * @return Model|null
     */
    public function findByMail($value) : ?Model;

    /**
     * @return Collection
     */
    public function all(): Collection;
}
