<?php

namespace App\Repository\Interfaces;

interface LogoutRepositoryInterface
{
    /**
     * @param $request
     */
    public function logout($request): void;
}
