<?php

namespace App\Repository\Interfaces;

use App\Exceptions\FailureResponse;
use App\Models\User;
use Illuminate\Http\Request;

interface LoginRepositoryInterface
{
    /**
     * @param Request $request
     * @param User $user
     * @return string|null
     * @throws FailureResponse
     */
    public function login(Request $request, User $user): string;
}
