<?php

namespace App\Repository\Interfaces;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface EloquentRepositoryInterface
{
    /**
     * @param $attribute
     * @param $value
     * @return Model|null
     */
    public function findBy($attribute, $value) : ?Model;
}
