<?php

namespace App\Repository\Interfaces;

interface ACLRepositoryInterface
{
    public function syncRole($user , $request): void;

    public function syncPermission($user , $request): void;
}
