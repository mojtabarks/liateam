<?php

namespace App\Repository\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface RegisterRepositoryInterface
{
    /**
     * @param $request
     * @return Model
     */
    public function register($request): Model;

    /**
     * @param $user
     */
    public function assignRole($user): void;

    /**
     * @param $user
     * @return string
     */
    public function createToken($user): string;
}
