<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;
use \Exception;

class FailureResponse extends Exception
{
    protected $data;
    protected $code;

    /**
     * FailureResponse constructor.
     * @param $data
     * @param $code
     */
    public function __construct($data, $code)
    {
        parent::__construct('The given data was invalid.' , $code);
        $this->data = $data;
        $this->code = $code;
    }

    /**
     * @return JsonResponse
     */
    public function render()
    {
        return response()->json([
            'success' => false,
            'code' => $this->code,
            'data' => $this->data,
        ], $this->code);
    }
}
