<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class ErrorMessage
{
    private $exception;

    /**
     * ErrorMessage constructor.
     * @param $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        $message = $this->exception->getMessage();

        if ($this->exception instanceof NotFoundHttpException) {
            $message = 'the requested uri not found .';
        }

        if ($this->exception instanceof MethodNotAllowedHttpException) {
            $message = 'method not allowed.';
        }

        return $message;
    }
}
