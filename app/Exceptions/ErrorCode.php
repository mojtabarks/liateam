<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class ErrorCode
{
    private $exception;

    /**
     * ErrorCode constructor.
     * @param $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        $code = $this->exception->getCode();

        if ($this->exception instanceof MethodNotAllowedHttpException) {
            $code = Response::HTTP_METHOD_NOT_ALLOWED;
        }

        if ($this->exception instanceof HttpException) {
            $code = $this->exception->getStatusCode();
        }

        if ($this->exception instanceof ModelNotFoundException) {
            $code = Response::HTTP_NOT_FOUND;
        }

        if ($this->exception instanceof ValidationException) {
            $code = $this->exception->status;
        }

        if ($code == 0) {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $code;
    }
}
