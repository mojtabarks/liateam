<?php

namespace App\Exceptions;

use App\Contracts\ReturnsResponse;
use Exception;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use App\Http\Response\FailureResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     * @return void
     *
     * @throws Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param $request
     * @param Throwable $exception
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if (method_exists($exception , 'render')) {
            return $exception->render();
        }

        return response()->json([
            'success' => false,
            'code' => (new ErrorCode($exception))->getCode(),
            'data' => $this->prepareErrorData($exception)
        ] , (new ErrorCode($exception))->getCode());
    }



    /**
     * @param Throwable $exception
     * @return string[]
     */
    private function prepareErrorData(Throwable $exception)
    {
        $error = [
            'message' => (new ErrorMessage($exception))->getMessage(),
        ];

        if ($exception instanceof ValidationException) {
            $error['errors'] = $exception->validator->errors()->all();
        }

        return $error;
    }
}
