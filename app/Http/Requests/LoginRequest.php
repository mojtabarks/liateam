<?php

namespace App\Http\Requests;

class LoginRequest extends FormRequest
{
    /**
     * @return mixed|string[]
     */
    protected function rules()
    {
        return [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ];
    }
}
