<?php

namespace App\Http\Requests;

class RegisterRequest extends FormRequest
{

    /**
     * @return mixed|string[]
     */
    protected function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}
