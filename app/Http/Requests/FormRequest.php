<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;

abstract class FormRequest extends Request
{
    /**
     * @return array|void
     * @throws ValidationException
     */
    public function validate () {
        if (false === $this->authorize()) {
            throw new UnauthorizedException();
        }

        $validator = Validator::make($this->all(), $this->rules(), $this->messages());

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * @return bool
     */
    protected function authorize () {
        return true;
    }

    /**
     * @return mixed
     */
    abstract protected function rules ();

    protected function messages () {
        return [];
    }
}
