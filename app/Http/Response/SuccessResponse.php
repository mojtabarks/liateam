<?php

namespace App\Http\Response;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SuccessResponse implements Responsable
{
    /**
     * @var mixed $data
     */
    private $data = null;

    /**
     * We need data to be array.
     *
     * @return array
     */
    public function getData()
    {
        if (!is_array($this->data)) {
            return [$this->data];
        }

        return $this->data;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        if (!is_array($data) && $data instanceof Arrayable) {
            $data = [$data];
        }

        $this->data = $data;
        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'success' => true,
            'code' => Response::HTTP_OK,
            'data' => $this->getData(),
        ]);
    }
}
