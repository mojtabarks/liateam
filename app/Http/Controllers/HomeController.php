<?php

namespace App\Http\Controllers;

use App\Http\Response\SuccessResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function secret(Request $request)
    {
        return (new SuccessResponse)
            ->setData('Hooray! you can see secret data!')
            ->toResponse($request);
    }
}
