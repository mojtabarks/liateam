<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Response\SuccessResponse;
use App\Repository\Interfaces\LogoutRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    /**
     * @var SuccessResponse
     */
    private $response;
    /**
     * @var LogoutRepositoryInterface
     */
    private $logoutRepository;

    /**
     * LogoutController constructor.
     * @param LogoutRepositoryInterface $logoutRepository
     * @param SuccessResponse $response
     */
    public function __construct(LogoutRepositoryInterface $logoutRepository, SuccessResponse $response)
    {
        $this->response = $response;
        $this->logoutRepository = $logoutRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
       $this->logoutRepository->logout($request);

        return $this->response
            ->setData(['message' => 'You have been successfully logged out!'])
            ->toResponse($request);
    }
}
