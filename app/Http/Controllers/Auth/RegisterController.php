<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterRequest;
use App\Repository\Auth\RegisterRepository;
use App\Http\Controllers\Controller;
use App\Http\Response\SuccessResponse;

class RegisterController extends Controller
{
    /**
     * @var SuccessResponse
     */
    private $response;
    /**
     * @var RegisterRepository
     */
    private $registerRepository;

    /**
     * RegisterController constructor.
     * @param RegisterRepository $registerRepository
     * @param SuccessResponse $response
     */
    public function __construct(RegisterRepository $registerRepository, SuccessResponse $response)
    {
        $this->response = $response;
        $this->registerRepository = $registerRepository;
    }

    /**
     * @param RegisterRequest $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->registerRepository->register($request);

        $token = $this->registerRepository->createToken($user);

        return $this->response
            ->setData([
                'email' => $user->getAttribute('email'),
                'token' => $token,
            ])->toResponse($request);
    }
}
