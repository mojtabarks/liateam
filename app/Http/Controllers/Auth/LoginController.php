<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\FailureResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Response\SuccessResponse;
use App\Models\User;
use App\Repository\Interfaces\LoginRepositoryInterface;
use App\Repository\Interfaces\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    /**
     * @var SuccessResponse
     */
    private $response;
    /**
     * @var LoginRepositoryInterface
     */
    private $loginRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * LoginController constructor.
     * @param UserRepositoryInterface $userRepository
     * @param LoginRepositoryInterface $loginRepository
     * @param SuccessResponse $response
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        LoginRepositoryInterface $loginRepository,
        SuccessResponse $response
    )
    {
        $this->response = $response;
        $this->loginRepository = $loginRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws FailureResponse
     */
    public function login(LoginRequest $request)
    {
        /** @var User $user */
        $user = $this->userRepository->findByMail($request->input('email'));

        $token = $this->loginRepository->login($request, $user);

        return $this->response
            ->setData([
                'email' => $user->getAttribute('email'),
                'token' => $token,
            ])
            ->toResponse($request);
    }
}
