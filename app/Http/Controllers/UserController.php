<?php

namespace App\Http\Controllers;

use App\Http\Response\SuccessResponse;
use App\Repository\Auth\ACLRepository;
use App\Repository\Eloquent\UserRepository;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;


class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ACLRepository
     */
    private $acl;
    /**
     * @var SuccessResponse
     */
    private $successResponse;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param ACLRepository $acl
     * @param SuccessResponse $successResponse
     */
    public function __construct(
        UserRepository $userRepository,
        ACLRepository $acl,
        SuccessResponse $successResponse
    )
    {
        $this->userRepository = $userRepository;
        $this->acl = $acl;
        $this->successResponse = $successResponse;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all();

        return (new SuccessResponse)
            ->setData($users)
            ->toResponse($request);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function grantPermission(Request $request)
    {
        $user = $this->userRepository->findByMail($request->input('user_email'));

        $this->acl->syncPermission($user, $request);

        $this->acl->syncRole($user, $request);

        return $this->successResponse
            ->setData($user->load('roles' , 'permissions'))
            ->toResponse($request);
    }
}
