<?php

namespace App\Models;

use \Spatie\Permission\Models\Role as MainRole;

class Role extends MainRole
{
    protected $hidden = ['pivot', 'guard_name' , 'created_at' , 'updated_at'];
}
