<?php

namespace App\Models;

use  Spatie\Permission\Models\Permission as MainPermission;

class Permission extends MainPermission
{
    protected $hidden = ['pivot', 'guard_name', 'created_at', 'updated_at'];
}
