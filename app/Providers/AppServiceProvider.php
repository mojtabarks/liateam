<?php

namespace App\Providers;

use App\Repository\Auth\ACLRepository;
use App\Repository\Auth\LoginRepository;
use App\Repository\Auth\LogoutRepository;
use App\Repository\Auth\RegisterRepository;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\UserRepository;
use App\Repository\Interfaces\ACLRepositoryInterface;
use App\Repository\Interfaces\EloquentRepositoryInterface;
use App\Repository\Interfaces\LoginRepositoryInterface;
use App\Repository\Interfaces\LogoutRepositoryInterface;
use App\Repository\Interfaces\RegisterRepositoryInterface;
use App\Repository\Interfaces\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

        $this->app->bind(LoginRepositoryInterface::class, LoginRepository::class);
        $this->app->bind(RegisterRepositoryInterface::class, RegisterRepository::class);
        $this->app->bind(LogoutRepositoryInterface::class, LogoutRepository::class);
        $this->app->bind(ACLRepositoryInterface::class, ACLRepository::class);
    }

    /**
     * Boot services for the application.
     *
     * @return void
     */
    public function boot()
    {
        \Dusterio\LumenPassport\LumenPassport::routes($this->app);


    }
}
