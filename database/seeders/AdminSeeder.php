<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $admin */
        $admin = User::create([
            'name' => 'name',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456789'),
            'remember_token' => Str::random(),
        ]);

        $adminRole = Role::where('name' , 'admin')->first();

        $admin->assignRole($adminRole);
    }
}
