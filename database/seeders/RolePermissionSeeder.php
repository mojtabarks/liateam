<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var HasRoles $adminRole */
        $adminRole = Role::create(['name' => 'admin']);

        /** @var HasRoles $userRole */
        $grantAccess = Permission::create(['name' => 'grant access']);

        $userRole = Role::create(['name' => 'user']);
        $getSecretData = Permission::create(['name' => 'get secret data']);

        $adminRole->givePermissionTo([$grantAccess , $getSecretData]);
        $userRole->givePermissionTo([$getSecretData]);
    }
}
