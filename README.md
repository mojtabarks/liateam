## Installation

1 - install docker and docker-compose on your system

2 - clone this repo

3 - `$ cd liateam/laradock`;

4 - `$ docker-compose run -d nginx mysql phpmyadmin`

5 - open `http://localhost:8081/` and create a database with name `liateam`

6 - in root folder `$ cp .env-example .env`

7 - in the .env : 

````
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=liateam
DB_USERNAME=root
DB_PASSWORD=root
````

8 - open `http://localhost` and you will see the welcome page
 
9 - `$ docker-compose exec workspace bash`

10 - `$ composer install`

11 - `$ php artisan migrate --seed`

12 - `$  php artisan passport:client --personal`

13 - you can use the `Liateam.postman_collection.json` file in the `storage/docs` 


